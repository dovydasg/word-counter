using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public static class Program
    {
        public static List<string> CountWordFrequency(string text, bool ignoreCase = true)
        {
            if (string.IsNullOrEmpty(text))
            {
                return new List<string>();
            }

            text = text.ToAlphaNumericOnly();

            IEnumerable<string> words = text.Split(' ');

            words = ignoreCase ? words : words.Select(w => w.ToLower()).ToList();

            var uniqueWords = words.GroupBy(g => g).Select(w => w.Key + ", " + w.Count()).ToList();

            return uniqueWords;
        }

        public static List<string> CountWordCountFrequency(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return new List<string>();
            }

            text = text.ToAlphaNumericOnly();

            IEnumerable<string> words = text.Split(' ');
            var uniqueWordCounts = words.Select(w => w.Count()).GroupBy(g => g).OrderBy(k => k.Key).Select(o => o.Key + ", " + o.Count()).ToList();

            return uniqueWordCounts;
        }
    }
}
