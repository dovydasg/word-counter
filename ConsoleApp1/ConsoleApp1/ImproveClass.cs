﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public static class ImproveClass
    {
        /* Method before
         * 
         * string foo(string argument)
        {
            string tmp;
            if (argument.Length == 1)
            {
                return argument;
            }
            else
            {
                string tempChar1 = argument.Substring(argument.Length - 1, argument.Length);
                string tempChar2 = argument.Substring(0, argument.Length - 1);
                tmp = tempChar1 + foo(tempChar2);
                return tmp;
            }
        }*/

        /// <summary>
        /// This function returns reversed string
        /// 
        /// However most efficient way to implement string reversal would be to convert string to 
        /// char array and use Array.Reverse function
        /// </summary>
        public static string foo(string argument)
        {
            string tmp;

            if (argument.Length > 1)
            {
                var lastChar = argument.Last();
                var remainingWord = argument.Substring(0, argument.Length - 1);
                tmp = lastChar + foo(remainingWord);
                argument = tmp;
            }

            return argument;
        }
    }
}
