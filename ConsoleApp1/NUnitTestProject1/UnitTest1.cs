using System;
using System.Collections.Generic;
using ConsoleApp1;
using NUnit.Framework;

namespace Tests
{
    public class Tests
    {
        private readonly string CorrectText = "I have a transportation device which is a red bike which I love to ride.";
        private readonly string EmptyText = string.Empty; // or null
        private readonly string RandomText = "Because book owners frequently have favorite passages that the books open themselves to, " +
                                             "some practitioners use dice or another randomiser to choose the page to be opened. This practice was formalized by the use of " +
                                             "coins or yarrow stalks in consulting the I Ching. Tarot divination can also be considered a form of bibliomancy, with the main " +
                                             "difference that the cards (pages) are unbound. " +
                                             "Another way around this is to cut the page with something like a razor.";
        private readonly string CaseSensitiveText = "So much Good Text is in here. Should we Stop writting? Or should We stop thinking? So what Is good happening over Here";

        [Test]
        public void Test_CountWordFrequency_With_Correct_Text()
        {
            var expectedList = new List<string> {"I, 2",
                                                "have, 1",
                                                "a, 2",
                                                "transportation, 1",
                                                "device, 1",
                                                "which, 2",
                                                "is, 1",
                                                "red, 1",
                                                "bike, 1",
                                                "love, 1",
                                                "to, 1",
                                                "ride, 1"};
            
            var list = Program.CountWordFrequency(CorrectText);

            Assert.IsNotNull(list);
            Assert.IsNotEmpty(list);

            Assert.AreEqual(list.Count, expectedList.Count);
            Assert.AreEqual(expectedList, list);
        }

        [Test]
        public void Test_CountWordCountFrequency_With_Correct_Text()
        {
            var expectedList = new List<string> {"1, 4",
                                                "2, 2",
                                                "3, 1",
                                                "4, 4",
                                                "5, 2",
                                                "6, 1",
                                                "14, 1"};

            var list = Program.CountWordCountFrequency(CorrectText);

            Assert.IsNotNull(list);
            Assert.IsNotEmpty(list);

            Assert.AreEqual(list.Count, expectedList.Count);
            Assert.AreEqual(expectedList, list);
        }

        [Test]
        public void Test_CountWordFrequency_With_Empty_Text()
        {
            var list = Program.CountWordFrequency(EmptyText);

            Assert.IsNotNull(list);
            Assert.IsEmpty(list);
        }

        [Test]
        public void Test_CountWordCountFrequency_With_Empty_Text()
        {
            var list = Program.CountWordCountFrequency(EmptyText);

            Assert.IsNotNull(list);
            Assert.IsEmpty(list);
        }

        [Test]
        public void Test_CountWordFrequency_With_Random_Text()
        {
            #region expectedList initialization
            var expectedList = new List<string> {"Because, 1",
                                                "book, 1",
                                                "owners, 1",
                                                "frequently, 1",
                                                "have, 1",
                                                "favorite, 1",
                                                "passages, 1",
                                                "that, 2",
                                                "the, 7",
                                                "books, 1",
                                                "open, 1",
                                                "themselves, 1",
                                                "to, 4",
                                                "some, 1",
                                                "practitioners, 1",
                                                "use, 2",
                                                "dice, 1",
                                                "or, 2",
                                                "another, 1",
                                                "randomiser, 1",
                                                "choose, 1",
                                                "page, 2",
                                                "be, 2",
                                                "opened, 1",
                                                "This, 1",
                                                "practice, 1",
                                                "was, 1",
                                                "formalized, 1",
                                                "by, 1",
                                                "of, 2",
                                                "coins, 1",
                                                "yarrow, 1",
                                                "stalks, 1",
                                                "in, 1",
                                                "consulting, 1",
                                                "I, 1",
                                                "Ching, 1",
                                                "Tarot, 1",
                                                "divination, 1",
                                                "can, 1",
                                                "also, 1",
                                                "considered, 1",
                                                "a, 2",
                                                "form, 1",
                                                "bibliomancy, 1",
                                                "with, 2",
                                                "main, 1",
                                                "difference, 1",
                                                "cards, 1",
                                                "pages, 1",
                                                "are, 1",
                                                "unbound, 1",
                                                "Another, 1",
                                                "way, 1",
                                                "around, 1",
                                                "this, 1",
                                                "is, 1",
                                                "cut, 1",
                                                "something, 1",
                                                "like, 1",
                                                "razor, 1"};
            #endregion

            var list = Program.CountWordFrequency(RandomText);

            Assert.IsNotNull(list);
            Assert.IsNotEmpty(list);

            Assert.AreEqual(list.Count, expectedList.Count);
            Assert.AreEqual(expectedList, list);
        }

        [Test]
        public void Test_CountWordCountFrequency_With_Random_Text()
        {
            var expectedList = new List<string> {"1, 3",
                                                "2, 13",
                                                "3, 14",
                                                "4, 17",
                                                "5, 7",
                                                "6, 6",
                                                "7, 4",
                                                "8, 3",
                                                "9, 1",
                                                "10, 8",
                                                "11, 1",
                                                "13, 1"};

            var list = Program.CountWordCountFrequency(RandomText);

            Assert.IsNotNull(list);
            Assert.IsNotEmpty(list);

            Assert.AreEqual(list.Count, expectedList.Count);
            Assert.AreEqual(expectedList, list);
        }

        [Test]
        public void Test_CountWordFrequency_With_Text_Case_Sensitive()
        {
            var expectedList = new List<string> {"so, 2",
                                                "much, 1",
                                                "good, 2",
                                                "text, 1",
                                                "is, 2",
                                                "in, 1",
                                                "here, 2",
                                                "should, 2",
                                                "we, 2",
                                                "stop, 2",
                                                "writting, 1",
                                                "or, 1",
                                                "thinking, 1",
                                                "what, 1",
                                                "happening, 1",
                                                "over, 1"};

            var list = Program.CountWordFrequency(CaseSensitiveText, false);

            Assert.IsNotNull(list);
            Assert.IsNotEmpty(list);

            Assert.AreEqual(list.Count, expectedList.Count);
            Assert.AreEqual(expectedList, list);
        }
    }
}